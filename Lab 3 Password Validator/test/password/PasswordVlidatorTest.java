package password;

/*
 * 
 * @author Luigi Agostino 991451048
 * 
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordVlidatorTest {
	
	//The testing for valid password length

	@Test
	public void testIsValidLengthRegular() {
		boolean isValidLength = PasswordValidator.isValidLength("HelloWorld");
		assertTrue("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello");
		assertFalse("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testIsValidLengthExceptionSpace() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello World");
		assertFalse("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testIsValidLengthExceptionNull() {
		boolean isValidLength = PasswordValidator.isValidLength(null);
		assertFalse("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testIsValidLengthBundaryOut() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello12");
		assertFalse("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testIsValidLengthBundaryIn() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello123");
		assertTrue("Invalid Password Length", isValidLength);
	}
	
	//Testing for password containing the recommend amount of digits
	
	@Test
	public void testHasValidDigitCountRegular() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("HelloWorld12");
		assertTrue("Not Enough digits!", hasValidDigits);
	}
	
	@Test
	public void testHasValidDigitCountException() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("HelloWorld");
		assertFalse("Not Enough digits!", hasValidDigits);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryIn() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("HelloWorld1");
		assertFalse("Not Enough digits!", hasValidDigits);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryOut() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("HelloWorld123");
		assertTrue("Not Enough digits!", hasValidDigits);
	}
	
	//Testing for upper and lower case characters
	@Test
	public void testhasUpperLowerRegualr() {
		boolean hasUpperLower = PasswordValidator.hasUpperLower("HelloWorld12");
		assertTrue("Not Enough upper/lower case characters!", hasUpperLower);
	}
	
	@Test
	public void testhasUpperLowerException() {
		boolean hasUpperLower = PasswordValidator.hasUpperLower("helloworld12");
		assertFalse("Not Enough upper/lower case characters!", hasUpperLower);
	}
	
	@Test
	public void testhasUpperLowerBoundaryIn() {
		boolean hasUpperLower = PasswordValidator.hasUpperLower("helloWorld1");
		assertTrue("Not Enough upper/lower case characters!", hasUpperLower);
	}
	
	@Test
	public void testhasUpperLowerBoundaryOut() {
		boolean hasUpperLower = PasswordValidator.hasUpperLower("HELLOWORLD123");
		assertFalse("Not Enough upper/lower case characters!", hasUpperLower);
	}
}
