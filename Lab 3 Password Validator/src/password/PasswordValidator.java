package password;

/*
 * 
 * @author Luigi Agostino 991451048
 * Assume spaces are not valid characters for the purpose of calculating length.
 * Assume all null passwords fail
 * 
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGIT = 2;
	
	public static boolean isValidLength (String password) {
		return (password != null && !password.contains(" ") && password.length() >= MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount (String password) {
		int count = 0;
		for(char c : password.toCharArray())
			if (Character.isDigit(c))
				count++;
		return (count >= MIN_DIGIT);
	}
	
	public static boolean hasUpperLower(String password) {
		int upperCount = 0;
		int lowerCount = 0;
		
		for(char c : password.toCharArray()) {
			if (Character.isUpperCase(c))
				upperCount++;
			if (Character.isLowerCase(c))
				lowerCount++;
		}
		return (lowerCount >=1 && upperCount >= 1);
	}
}
